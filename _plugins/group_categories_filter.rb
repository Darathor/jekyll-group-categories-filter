# @name         Group categories filter
# @version      1.0.0
# @author       Darathor < darathor@free.fr >
# @supportURL   https://framagit.org/Darathor/jekyll-group-categories-filter/issues
# @license      WTFPL

module Jekyll
  module GroupCategories
    @@category_groups = {}
    @@default_group_category = 'Default'

    # Config
    def self.loadConfig(site)
      @@category_groups = site.config['category_groups']
      if site.config.has_key?('default_group_category')
        @@default_group_category =  site.config['default_group_category']
      end
    end

    def group_categories(hashTable)
      groupedCategories = Hash.new;

      reversedDefinitions = Hash.new
      @@category_groups.each_key do |key|
        groupedCategories[key] = Hash.new
        @@category_groups[key].each do |value|
          reversedDefinitions[value] = key
        end
      end
      groupedCategories[@@default_group_category] = Hash.new;

      hashTable.each do |item|
        key = item[0]
        if reversedDefinitions.has_key?(key) && groupedCategories.has_key?(reversedDefinitions[key])
          groupedCategories[reversedDefinitions[key]][key] = item[1]
        else
          groupedCategories[@@default_group_category][key] = item[1]
        end
      end

      return groupedCategories;
    end
  end
end

Jekyll::Hooks.register :site, :pre_render do |site, payload|
  Jekyll::GroupCategories::loadConfig(site)
end

Liquid::Template.register_filter(Jekyll::GroupCategories)