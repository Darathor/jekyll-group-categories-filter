# jekyll-hash-table-sort-filters

## Français

Il s'agit d'un plugin [Jekyll](https://jekyllrb.com/) ajoutant un nouveau filtre dans les templates Liquid destiné réaliser un menu de catégories groupées en "super-catégories".

### Installation

Copier le fichier `_plugins/group_categories_filter.rb` dans le répertoire `_plugins` de votre site Jekyll et relancer le serveur Jekyll.

### Configuration

Dans le fichier `_config.yml` du projet, deux nouvelles entrées de configiration sont disponibles :
 * `category_groups` : table de hachage permettant de définir les groupes (la clé de chaque entrée est le libellé du groupe et son contenu le tableau des noms de catégories formant le groupe), tout ce qui n'est présent dans aucun groupe sera intégré à un groupe par défaut
 * `default_group_category` : permet de définir le libellé du groupe par défaut où se retrouveront les catégories n'ayant été affectées à aucun groupe

Exemple :
```
category_groups:
  Jeux:
    - 'Magic'
    - 'Jeux vidéos'
    - 'Jeux de société'
  'Réalisations perso':
    - 'Développements perso'
    - 'Créations perso'
    - 'À propos du blog'
    - 'Mes autres sites'

default_group_category: 'Autres'
```

### Utilisation

Exemple :
```
<h2>Catégories</h2>
{% assign groupedCategories = site.categories | group_categories %}
{% for group in groupedCategories %}
  <h3>{{ group[0] }}</h3>
  <ul>
    {% assign categories = group[1] %}
    {% for category in categories %}
      <li><a href="/categories/{{ category[0]|slugify:'latin' }}/">{{ category[0] }}</a> ({{ category[1].size }})</li>
    {% endfor %}
  </ul>
{% endfor %}
```

## Anglais

This is a plugin for [Jekyll](https://jekyllrb.com/) that adds a new filter usable in Liquid templates to group categories.

### Installation

Copy `_plugins/group_categories_filter.rb` into the `_plugins` directory of Jekyll site working directory and restart the Jekyll server.

### Configuration

In the project `_config.yml` file, two new configuration entries are available:
  * `category_groups`: hash table to define the groups (the key of each entry is the label of the group and its contents the array of the names of categories forming the group), all that is not present in any group will be integrated to a default group
  * `default_group_category`: sets the default group label where categories that have not been assigned to any group will be found

Example:
```
category_groups:
  Games:
    - 'Magic'
    - 'Video games'
  'Personal realisations':
    - 'Personal developments'
    - 'About this blog'
    - 'My other websites'

default_group_category: 'Other'
```

### Usage

Example :
```
<h2>Categories</h2>
{% assign groupedCategories = site.categories | group_categories %}
{% for group in groupedCategories %}
  <h3>{{ group[0] }}</h3>
  <ul>
    {% assign categories = group[1] %}
    {% for category in categories %}
      <li><a href="/categories/{{ category[0]|slugify:'latin' }}/">{{ category[0] }}</a> ({{ category[1].size }})</li>
    {% endfor %}
  </ul>
{% endfor %}
```